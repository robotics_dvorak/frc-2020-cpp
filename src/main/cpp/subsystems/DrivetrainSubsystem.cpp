/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#include "subsystems/DrivetrainSubsystem.h"
#include "commands/TeleopDrive.h"
#include "Constants.h"

using namespace ctre::phoenix::motorcontrol;

DrivetrainSubsystem::DrivetrainSubsystem( frc::XboxController* controller) {
  driver_controller = controller;
  motorLF = new TalonFX(Drivetrain::LF);
  motorRF = new TalonFX(Drivetrain::RF);
  motorLB = new TalonFX(Drivetrain::LB);
  motorRB = new TalonFX(Drivetrain::RB);
}

void DrivetrainSubsystem::InitDefaultCommand() {
  TeleopDrive default_command = *(new TeleopDrive(this, driver_controller));
  Subsystem::SetDefaultCommand( default_command );
}

void DrivetrainSubsystem::arcade_drive(double straight, double rotate) {
  double left_speed = straight - rotate;
  double right_speed = straight + rotate;

  // Deadzone
  if (abs(left_speed) < Drivetrain::min_motor_output)
    left_speed = 0;
  if (abs(right_speed) < Drivetrain::min_motor_output)
    right_speed = 0;

  motorLF->Set(ControlMode::PercentOutput, left_speed);
  motorLB->Set(ControlMode::PercentOutput, left_speed);
  motorRF->Set(ControlMode::PercentOutput, right_speed);
  motorRB->Set(ControlMode::PercentOutput, right_speed);
}
