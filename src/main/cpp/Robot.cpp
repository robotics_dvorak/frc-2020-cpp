/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#include "Robot.h"

#include <frc/smartdashboard/SmartDashboard.h>
#include <frc2/command/CommandScheduler.h>

using namespace Helpers;

void Robot::RobotInit() {
  firstController = new frc::XboxController(0);

  // Auton Chooser
  auton_chooser = new frc::SendableChooser< AutonChoice >();
  auton_chooser->SetDefaultOption("default", AutonChoice::DO_NOTHING);
  // auton_chooser->AddOption("Drive Forward", "drive_forward"); // TODO add drive_forward option
}

/**
 * This function is called every robot packet, no matter the mode. Use
 * this for items like diagnostics that you want to run during disabled,
 * autonomous, teleoperated and test.
 *
 * <p> This runs after the mode specific periodic functions, but before
 * LiveWindow and SmartDashboard integrated updating.
 */
void Robot::RobotPeriodic() { frc2::CommandScheduler::GetInstance().Run(); }

/**
 * This function is called once each time the robot enters Disabled mode. You
 * can use it to reset any subsystem information you want to clear when the
 * robot is disabled.
 */
void Robot::DisabledInit() {}

void Robot::DisabledPeriodic() {}

void Robot::AutonomousInit() {
  //AutonChoice selected_auto = auton_chooser->GetSelected();
  //Auton* auton_command = new Auton( AutonChoice::DO_NOTHING );

  if (auton_command != nullptr) { // Sanity check to not schedule a nullptr
    auton_command->Schedule();
  }
}

void Robot::AutonomousPeriodic() {}

void Robot::TeleopInit() {
  if (auton_command != nullptr) { // Cancel auton
    auton_command->Cancel();
    auton_command = nullptr;
  }
}

void Robot::TeleopPeriodic() {}
