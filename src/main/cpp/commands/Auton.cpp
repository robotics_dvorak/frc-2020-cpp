/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#include "commands/Auton.h"

Auton::Auton ( AutonChoice auto_selection ) {
  auton_selection = auto_selection;
}

void Auton::Execute () {
  if ( auton_selection == AutonChoice::DO_NOTHING )
    return;
}

bool Auton::IsFinished () {
  return true;
}
