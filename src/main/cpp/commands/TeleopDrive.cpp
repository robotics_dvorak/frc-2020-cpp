/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#include "commands/TeleopDrive.h"

TeleopDrive::TeleopDrive (DrivetrainSubsystem* subsystem, frc::XboxController* driver) {
  drivetrain_subsys = subsystem;
  controller = driver;
}

void TeleopDrive::Execute () {
}
