/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#pragma once

#include <frc/Joystick.h>
#include <frc/GenericHID.h>

/*

  The Constants header provides a convenient place for teams to hold robot-wide
  numerical or boolean constants.  This should not be used for any other
  purpose.

*/

namespace Drivetrain {
  constexpr int LF = 10;
  constexpr int RF = 11;
  constexpr int LB = 7;
  constexpr int RB = 9;
  constexpr double min_motor_output = 0.2; // 20%
}

namespace Helpers {
  enum class AutonChoice { DO_NOTHING };
}
