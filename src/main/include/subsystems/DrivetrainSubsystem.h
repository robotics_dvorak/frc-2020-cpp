/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#pragma once

#include "ctre/Phoenix.h"
#include <frc2/command/SubsystemBase.h>
#include <frc/XboxController.h>

class DrivetrainSubsystem : public frc2::SubsystemBase {
  public:
    DrivetrainSubsystem( frc::XboxController* controller );
    void InitDefaultCommand();
    void arcade_drive( double straight, double rotate);
    // void drive_inches( double inches ); // TODO use 'units.h'

  private:
    TalonFX* motorLF;
    TalonFX* motorRF;
    TalonFX* motorLB;
    TalonFX* motorRB;

    frc::XboxController* driver_controller;
};
