/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#pragma once

#include <frc/TimedRobot.h>

// FRC includes
#include <frc2/command/Command.h>
#include <frc/XboxController.h>
#include <frc/smartdashboard/SendableChooser.h>

// Utility includes
#include "Constants.h"
// Subsytem includes
#include "subsystems/DrivetrainSubsystem.h"
// Command includes
#include "commands/Auton.h"

class Robot : public frc::TimedRobot {
  public:
    void RobotInit() override;
    void RobotPeriodic() override;
    void DisabledInit() override;
    void DisabledPeriodic() override;
    void AutonomousInit() override;
    void AutonomousPeriodic() override;
    void TeleopInit() override;
    void TeleopPeriodic() override;

  private:
    // Auton things
    frc::SendableChooser< AutonChoice >* auton_chooser;
    Auton* auton_command = nullptr;

    // Subsystems
    DrivetrainSubsystem* drivetrain_subsys;

    // Controllers
    frc::XboxController* firstController;
};
